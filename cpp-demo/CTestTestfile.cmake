# CMake generated Testfile for 
# Source directory: /home/bryan/Projets/gitlabci-demo-cpp/cpp-demo
# Build directory: /home/bryan/Projets/gitlabci-demo-cpp/cpp-demo
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(DemoAppTest "/home/bryan/Projets/gitlabci-demo-cpp/cpp-demo/src/DemoAppTest/DemoAppTest")
subdirs("src/DemoApp")
subdirs("src/DemoAppTest")
